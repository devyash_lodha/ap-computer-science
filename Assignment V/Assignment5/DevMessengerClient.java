import java.net.*;
import java.io.*;
import acm.program.*;
import acm.util.*;
import acm.graphics.*;
import acm.gui.*;

public class DevMessengerClient extends ConsoleProgram
{
	Socket socket;
	ObjectOutputStream out;
	ObjectInputStream in;
	private String hostname;
	private RandomGenerator rgen = RandomGenerator.getInstance();
	
	public void init()
	{
		setSize(640, 480);
		hostname = "localhost";
		try
		{
			socket = new Socket(hostname, 54321);
			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
		} catch(IOException ex) {
			println("Socket connection/creation failed!");
		}
	}
	
	public void run()
	{
		println("Welcome to the P2P Messenger, by Devyash!");
		String node = readLine("Please enter a name!");
		while(true)
		{
			int num = rgen.nextInt();
			String message = node + ">> Number: " + Integer.toString(num);
			println(message);
		}
	}
}