// File Name GreetingServer.java

import java.net.*;
import java.io.*;
import acm.program.*;
import acm.util.*;

public class DevSocketServerExample extends ConsoleProgram
{
	private ServerSocket server;
	private static final int port = 54321;
	
	public void run()
	{
		try
		{
			server = new ServerSocket(port);
		} catch(IOException ex) {
			println("Count not start server. Perhaps, there is a server on the same port preventing the binding");
		}
		while(true)
		{
			try
			{
				println("Waiting for a client to connect");
				Socket socket = server.accept();
				ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
				String message = (String) in.readObject();
				println("Recieved: " + message);
				ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
				out.writeObject("Hello World! " + message);
				out.close();
				in.close();
				socket.close();
				if(message.equalsIgnoreCase("exit"))
				{
					break;
				}
			} catch(IOException ex) {
				println("Something went wrong! Perhaps, the socket timed out!");
			} catch(ClassNotFoundException ex) {
				println("Something went wrong!");
			}
		}
		try
		{
		server.close();
		} catch(IOException ex) {
			println("Something went wrong here!");
		}
	}
}