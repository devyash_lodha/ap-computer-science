import java.net.*;
import java.io.*;
import acm.program.*;
import acm.util.*;

public class DevSocketExample extends ConsoleProgram
{
	InetAddress host;
	Socket socket;
	ObjectOutputStream out;
	ObjectInputStream in;
	
	public void init()
	{
		try
		{
			host = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		for(int i = 0; i < 5; i++)
		{
			try
			{
				socket = new Socket(host.getHostName(), 54321);
				out = new ObjectOutputStream(socket.getOutputStream());
				println("Senting request to Socket Server!");
				if(i == 4)
				{
					out.writeObject("exit");
				} else {
					out.writeObject("Number: " + i);
				}
				in = new ObjectInputStream(socket.getInputStream());
				String message = (String) in.readObject();
				println("Message: " + message);
				in.close();
				out.close();
			} catch(IOException ex) {
				println("Something went wrong!");
			} catch(ClassNotFoundException ex) {
				println("Something went wrong again!");
			}
			pause(100);
		}
	}
}