/*
 * File: Yahtzee.java
 * ------------------
 * This program will eventually play the Yahtzee game.
 */

import acm.io.*;
import acm.program.*;
import acm.util.*;
import acm.graphics.*;
import java.util.*;
import java.net.*;
import java.io.*;

//Yahtzee main (Devyash Lodha)

public class Yahtzee extends GraphicsProgram implements YahtzeeConstants
{
	//initialise the program
	public static void main(String[] args)
	{
		new Yahtzee().start(args);
	}

	public void init()
	{
		//set the window size so the user doesn't have to
		setSize(640, 480);
		//get the number of players
		playerCount = dialog.readInt("Number of players: ");
		//validates the player count
		while(true)
		{
			if(playerCount <= MAX_PLAYERS)
			{
				break;
			}
			playerCount = dialog.readInt("Number of players: ");
		}
		
		//allocate space for the data
		//--playerNames holds the player's actual name
		playerNames = new String[playerCount];
		//--the catScores holds category score data, to store all the scores
		catScores = new int[playerCount + 1][N_CATEGORIES + 1];
		//--selectedCategories holds the categories that have been selected
		selectedCategories = new int[playerCount + 1][N_CATEGORIES + 1];
		
		//asks for the player's names
		for(int i = 1; i <= playerCount; i++)
		{
			//this stores the name, for validation
			String name = "";
			//loop until valid input
			while(true)
			{
				//get the name
				name = dialog.readLine("Enter the player name [" + i + "]");
				//trim whitespace
				name.trim();
				//check to make sure the name is not empty
				if(name.equals(""))
				{
					break;
				}
			}
			//assign the name to the String
			playerNames[i-1] = name;
		}
		
		//create the display and the canvas
		display = new YahtzeeDisplay(getGCanvas(), playerNames);
	}
	
	//the run method. It is run after the init method
	public void run()
	{
		for(int a = 0; a < N_SCORING_CATEGORIES; a++)
		{
			for(int b = 1; b <= playerCount; b++)
			{
				initialiseFirstRoll(b);
				secondAndThirdRoll(b);
				selectCategory(b);
			}
		}
		calculateResults();
		calculateWinner();
	}
		
	private void initialiseFirstRoll(int player)
	{
		for(int i = 0; i < N_DICE; i++)
		{
			dieResults[i] = rgen.nextInt(1, 6);
		}
		
		display.printMessage(playerNames[player - 1] + "'s turn! Roll the dice, please!");
		display.waitForPlayerToClickRoll(player);
		display.displayDice(dieResults);
	}
	
	private void secondAndThirdRoll(int player)
	{
		for(int a = 0; a < 2; a++)
		{
			display.printMessage("You need to select dice to re-roll!");
			display.waitForPlayerToSelectDice();
			for(int b = 0; b < N_DICE; b++)
			{
				if(display.isDieSelected(b))
				{
					dieResults[b] = rgen.nextInt(1, 6);
				}
			}
			display.displayDice(dieResults);
		}
	}
	
	private void selectCategory(int player)
	{
		display.printMessage("Select a category for this roll!");
		while(true)
		{
			category = display.waitForPlayerToSelectCategory();
			if(selectedCategories[player][category] == 0)
			{
				calculateCategoryScore(player);
				break;
			}
			display.printMessage("You have already selected that category! :(");
		}
	}
	
	private void calculateCategoryScore(int player)
	{
		selectedCategories[player][category] = 1;
		int totalScore = 0;
		if(checkCategory(dieResults, category))
		{
			setCategoryScore(player, category);
			int score = catScores[player][category];
			display.updateScorecard(category, player, score);
			calculateTotalScores(player);
			totalScore = catScores[player][TOTAL];
			display.updateScorecard(TOTAL, player, totalScore);
		} else {
			catScores[player][category] = 0;
			display.updateScorecard(category, player, totalScore);
			calculateTotalScores(player);
			totalScore = catScores[player][TOTAL];
			display.updateScorecard(TOTAL, player, totalScore);
		}
	}
	
	private void setCategoryScore(int player, int category)
	{
		int score = 0;
		if(category >= ONES && category <= SIXES)
		{
			for(int i = 0; i < N_DICE; i++)
			{
				if(dieResults[i] == category)
				{
					score += category;
				}
			}
		} else {
			if(category == THREE_OF_A_KIND || category == FOUR_OF_A_KIND || category == CHANCE)
			{
				for(int i = 0; i < N_DICE; i++)
				{
					score += category;
				}
			} else {
				if(category == FULL_HOUSE)
				{
					score = 25;
				} else {
					if(category == SMALL_STRAIGHT)
					{
						score = 30;
					} else {
						if(category == LARGE_STRAIGHT)
						{
							score = 40;
						} else {
							if(category == YAHTZEE)
							{
								score = 50;
							}
						}
					}
				}
			}
		}
		
		catScores[player][category] = score;
	}
	
	private void calculateTotalScores(int player)
	{
		int upperScore = 0;
		int lowerScore = 0;
		int totalScore = 0;
		
		for(int i = ONES; i <= SIXES; i++)
		{
			upperScore += catScores[player][i];
		}
		
		for(int i = THREE_OF_A_KIND; i <= CHANCE; i++)
		{
			lowerScore += catScores[player][i];
		}
		
		totalScore = upperScore + lowerScore;
		
		catScores[player][UPPER_SCORE] = upperScore;
		catScores[player][LOWER_SCORE] = lowerScore;
		catScores[player][TOTAL] = totalScore;
	}
	
	private void calculateResults()
	{
		for(int i = 1; i <= playerCount; i++)
		{
			display.updateScorecard(UPPER_SCORE, i, catScores[i][UPPER_SCORE]);
			display.updateScorecard(LOWER_SCORE, i, catScores[i][LOWER_SCORE]);
			
			if(catScores[i][UPPER_SCORE] >= 63)
			{
				catScores[i][UPPER_BONUS] = 35;
			}
			
			display.updateScorecard(TOTAL,  i,  catScores[i][TOTAL]);
		}
	}
	
	private void calculateWinner()
	{
		int winningScore = 0;
		int winningPlayer = 0;
		for(int i = 1; i <= playerCount; i++)
		{
			int k = catScores[i][TOTAL];
			if(k > winningScore)
			{
				winningScore = k;
				winningPlayer = i - 1;
			}
		}
		
		display.printMessage("Congrats, " + playerNames[winningPlayer] + "! You have won!");
	}
	
	private boolean checkCategory(int[] dice, int category)
	{
		boolean categoryMatch = false;
		if(category >= ONES && category <= SIXES || category == CHANCE)
		{
			categoryMatch = true;
		} else {
			Vector<Integer> ones = new Vector<Integer>(1, 1);
			Vector<Integer> twos = new Vector<Integer>(1, 1);
			Vector<Integer> threes = new Vector<Integer>(1, 1);
			Vector<Integer> fours = new Vector<Integer>(1, 1);
			Vector<Integer> fives = new Vector<Integer>(1, 1);
			Vector<Integer> sixes = new Vector<Integer>(1, 1);
			
			for(int i = 0; i < N_DICE; i++)
			{
				if(dice[i] == 1)
				{
					ones.add(1);
				} else {
					if(dice[i] == 2)
					{
						twos.add(1);
					} else {
						if(dice[i] == 3)
						{
							threes.add(1);
						} else {
							if(dice[i] == 4)
							{
								fours.add(1);
							} else {
								if(dice[i] == 5)
								{
									fives.add(1);
								} else {
									if(dice[i] == 6)
									{
										sixes.add(1);
									}
								}
							}
						}
					}
				}
			}
			
			if(category == THREE_OF_A_KIND)
			{
				if(ones.size() >= 3 || twos.size() >= 3 || threes.size() >= 3 || fours.size() >= 3 || fives.size() >= 3 || sixes.size() >= 3)
				{
					categoryMatch = true;
				} else {
					if(category == FOUR_OF_A_KIND)
					{
						if(ones.size() >= 4 || twos.size() >= 4 || threes.size() >= 4 || fours.size() >= 4 || fives.size() >= 4 || sixes.size() >= 4)
						{
							categoryMatch = true;
						}
					} else {
						if(category == YAHTZEE)
						{
							if(ones.size() == 5 || twos.size() == 5 || threes.size() == 5 || fours.size() == 5 || fives.size() == 5 || sixes.size() == 5)
							{
								categoryMatch = true;
							}
						} else {
							if(category == FULL_HOUSE)
							{
								if(ones.size() == 6 || twos.size() == 6 || threes.size() == 5 || fours.size() == 6 || fives.size() == 6 || sixes.size() == 6)
								{
									categoryMatch = true;
								}
							} else {
								if(category == LARGE_STRAIGHT)
								{
									if(ones.size() == 1 && twos.size() == 1 && threes.size() == 1 && fours.size() == 1 && fives.size() == 1)
									{
										categoryMatch = true;
									} else {
										if(twos.size() == 1 && threes.size() == 1 && fours.size() == 1 && fives.size() == 1 && sixes.size() == 1)
										{
											categoryMatch = true;
										}
									}
								} else {
									if(category == SMALL_STRAIGHT)
									{
										if(ones.size() >= 1 && twos.size() >= 1 && threes.size() >= 1 && fours.size() >= 1)
										{
											categoryMatch = true;
										} else {
											if(twos.size() >= 1 && threes.size() >= 1 && fours.size() >= 1 && fives.size() >= 1)
											{
												categoryMatch = true;
											} else {
												if(threes.size() >= 1 && fours.size() >= 1 && fives.size() >= 1 && sixes.size() >= 1)
												{
													categoryMatch = true;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return categoryMatch;
	}
	
	//The dialog we will use
	private IODialog dialog = getDialog();
	//number of players
	private int playerCount;
	//the names of the players
	private String[] playerNames;
	//the main application window
	private YahtzeeDisplay display;
	//the random number generator
	private RandomGenerator rgen = RandomGenerator.getInstance();
	//the category we have selected
	private int category;
	//the dies
	private int[] dieResults = new int[N_DICE];
	//the category scores
	private int[][] catScores;
	//we need to store all the categories we have already used!
	private int[][] selectedCategories;
	//is it the first roll?
	private boolean firstRoll = false;
}
