/******************************************
 * @author=Devyash Lodha                  *
 * @Program=finds lower and higher numbers*
 ******************************************/
import acm.program.*;
	
public class FindRange extends ConsoleProgram {
	public void run() {
		//Tells What to Do!
		println("This Program Finds The Largest And The Smallest Numbers.\n\tPress \"0\" to exit");
		//lowN is the variable for the lower number; highN is for the highest number
		int lowN,highN;
		//get input
		int in=readInt("root@computer #");
		//assign starting values to the high and low numbers because there will only be one number
		highN=in;
		lowN=in;
		println("\n");
		//infinite loop until the break statement is triggered
		while(true){
			//get input
			in=readInt("root@computer # ");
			//check to see if the user put a zero and break
			if(in==0){
				break;
			}
			//to make sure the number is equal to the starting value
			if(in==highN&&in==lowN){
				highN=in;
				lowN=in;
			}else{
				//if the number is lower
				if(in<lowN){
					lowN=in;
				}else{
					//if the number is higher
					if(in>highN){
						highN=in;
					}
				}
			}
		}
		//print results
		println("\tLow ="+lowN+"\n\tHigh ="+highN);
	}
}