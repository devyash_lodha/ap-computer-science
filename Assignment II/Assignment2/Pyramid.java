/*
 * File: Pyramid.java
 * Name: 
 * Section Leader: 
 * ------------------
 * This file is the starter file for the Pyramid problem.
 * It includes definitions of the constants that match the
 * sample run in the assignment, but you should make sure
 * that changing these values causes the generated display
 * to change accordingly.
 */

import acm.graphics.*;
import acm.program.*;
import java.awt.*;

public class Pyramid extends GraphicsProgram {

/** Width of each brick in pixels */
	private static final int BRICK_WIDTH = 30;

/** Width of each brick in pixels */
	private static final int BRICK_HEIGHT = 12;

/** Number of bricks in the base of the pyramid */
	private static final int BRICKS_IN_BASE = 14;
	
	public void run(){
		int sw=640;
		int sh=480;
		int midw=sw/2;
		int midh=sh/2;
		int height=0;
		setSize(sw,sh);
		//int boundary=(sw-(BRICK_WIDTH*brickRow))/2;
		//int margin=boundary;
		int row=0;
		int length=BRICKS_IN_BASE;
		int rows=length/1;
// 		/*for(int a=0;a<rows;a++){
// 			for(int b=0;b<brickRow;b++){
// 				newBrick(boundary,height);
// 				boundary+=BRICK_WIDTH;
// 			}
//			//boundary=margin+(BRICK_WIDTH*(a));
// 			boundary=margin;
// 			boundary+=(a*BRICK_WIDTH);
//			height+=BRICK_HEIGHT;
//		}*/
//		int l=brickRow;
//		for(int i=0;i<rows;i++){
//			layer(l,row);
//			row+=BRICK_HEIGHT;
//			l-=BRICK_WIDTH;
//		}
		for(int i=0;i<rows;i++){
			layer(length,height,margin);
			height+=BRICK_HEIGHT;
			length+=BRICK_WIDTH;
		}
	}
	
	private void newBrick(int x,int y){
		add(new GRect(x,y,BRICK_WIDTH,BRICK_HEIGHT));
	}
	
	private void layer(int length,int height,int m){
		int x=length*BRICK_WIDTH;
		int margin=(640-x)/2;
		for(int i=0;i<x;i++){
			newBrick(margin,height);
			margin+=BRICK_WIDTH;
		}
	}
}