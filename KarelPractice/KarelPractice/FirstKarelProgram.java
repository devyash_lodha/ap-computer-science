

/*
 * File: FirstKarelProgram.java
 * ----------------------------
 * This program solves the problem of moving a beeper to a ledge.
 */

import stanford.karel.*;

public class FirstKarelProgram extends SuperKarel {
	
//	private int counter;
//	public void run() {
//		for( int counter = 0; counter < 8; counter++ ) {
//			moveToWall();
//			climbSteeple();
//		}
//	}	
//	
//	private int moveForward(int distance) {
//		for( counter = 0; counter < distance; counter++ ) {
//			if( frontIsClear() ) {
//				move();
//			} else {
//				return -1;
//			}
//		}
//		return 0;
//	}
//	
//	private int climbSteeple() {
//		turnLeft();
//		if( rightIsBlocked() ) {
//			moveForward(1);
//		}
//		turnRight();
//		moveForward(1);
//		turnRight();
//		moveToWall();
//		return 0;
//	}
//	
//	private int moveToWall() {
//		while( frontIsClear() ) {
//			moveForward(1);
//		}
//		return 0;
//	}
//	
//	
//	
//	
//	public void run() {
//		for(int i = 0; i < 8; i++) {
//			if(frontIsClear()) {
//				move();
//			} else {
//				jumpHurdle();
//			}
//		}
//	}
//	private void jumpHurdle() {
//		ascendHurdle();
//		move();
//		descendHurdle();
//	}
//	private void ascendHurdle() {
//		turnLeft();
//		while(rightIsBlocked()) {
//			move();
//		}
//		turnRight();
//	}
//	private void descendHurdle() {
//		turnRight();
//		moveToWall();
//		turnLeft();
//	}
//	private void moveToWall() {
//		while( frontIsClear() ) {
//			move();
//		}
//	}
//
//	public void run() {
//		createBeeperLine();
//		putBeeper();
//	}
//	private void createBeeperLine() {
//		while( frontIsClear() ) {
//			putBeeper();
//			move();
//		}
//	}
//
//	public void run() {
//		cleanRow();
//		while(leftIsClear()) {
//			repositionForRowToWest();
//			cleanRow();
//			if(rightIsClear() ) {
//				repositionForRowToEast();
//				cleanRow();
//			} else {
//				turnAround();
//			}
//		}
//	}
//	private void repositionForRowToWest() {
//		turnLeft();
//		move();
//		turnLeft();
//	}
//	
//	private void cleanRow() {
//		if(beepersPresent()) {
//			pickBeeper();
//		}
//		while(frontIsClear()) {
//			move();
//			if(beepersPresent()) {
//				pickBeeper();
//			}
//		}
//	}
//	private void repositionForRowToEast() {
//		turnRight();
//		move();
//		turnRight();
//	}
//
//
	public void run() {
		 for( int i = 0; i < 16; i++ ) {
			 move();
		 
			 doubleBeepersInPile();
			 moveBackwards();
		 }
	}
	
	private void doubleBeepersInPile() {
		while( beepersPresent() ) {
			pickBeeper();
			putTwoBeepersNextDoor();
		}
		movePileNextDoorBack();
	}
	
	private void putTwoBeepersNextDoor() {
		move();
		putBeeper();
		putBeeper();
		moveBackwards();
	}
	
	private void movePileNextDoorBack() {
		move();
		while( beepersPresent() ) {
			moveOneBeeperBack();
		}
		moveBackwards();
	}
	
	private void moveOneBeeperBack() {
		pickBeeper();
		moveBackwards();
		putBeeper();
		move();
	}
	
	private void moveBackwards() {
		turnAround();
		move();
		turnAround();
	}
}
