/*
 * File: HangmanLexicon.java
 * -------------------------
 * This file contains a stub implementation of the HangmanLexicon
 * class that you will reimplement for Part III of the assignment.
 */

import acm.util.*;
import java.io.*;
import java.util.*;
import acm.program.*;

public class HangmanLexicon extends ConsoleProgram {

/** Returns the number of words in the lexicon. */
	public int getWordCount() {
		return 10;
	}

/** Returns the word at the specified index. */
	public String getWord(int index) {

		BufferedReader wordGen = null;
		try {
			wordGen = new BufferedReader(new FileReader("/home/share/config.devx"));
		} catch (FileNotFoundException e) {
			println("Lexicon not found!");
			e.printStackTrace();
		}
		
		boolean cont = true;
		ArrayList words = new ArrayList<String>();
		while(cont) {
			try {
				String w = wordGen.readLine();
				words.add(w);
			} catch(IOException e) {}
		}
		try {
			wordGen.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(index < 0 || index > words.size()) {
			return null;
		} else {
			return words.get(index).toString();
		}
	};
}
