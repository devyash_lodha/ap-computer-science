/*
 * File: Hangman.java
 * ------------------
 * This program will eventually play the Hangman game from
 * Assignment #4.
 */

import acm.graphics.*;
import acm.program.*;
import acm.util.*;
import java.awt.*;

public class Hangman extends ConsoleProgram {
	HangmanLexicon hl = new HangmanLexicon();
    public void run() {
		while(true) {
			println("# Welcome to Hangman!");
			int index = readInt("# Please enter a number!\n# ");
			println("# " + hl.getWord(index));
		}
	}
}
