/**************
 * This is a hailstones program to make a number reach one :D
 **************/

import acm.program.*;

public class Hailstone extends ConsoleProgram {
	public void run() {
		println("root@computer # ./hailstones");
		int number=readInt("Enter a number ");
		int counter=0;
		while(true){
			counter++;
			if(number%2==0){
				println(number+" is even, so I take half: "+number/2);
				number/=2;
			}else{
				println(number+" is odd, so I make 3n+1:"+(number*3)+1);
				number*=3;
				number++;
			}
			if(number==1){
				break;
			}
		}
		println("This took "+counter+" steps to reach 1");
	}
}