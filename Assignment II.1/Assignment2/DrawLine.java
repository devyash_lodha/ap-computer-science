import java.awt.event.MouseEvent;

import acm.graphics.*;
import acm.program.*;
import acm.util.*;

public class DrawLine extends GraphicsProgram {
	int x1=0;
	int y1=0;
	int x2=0;
	int y2=0;
	
	boolean one = false;
	public void run() {
		addMouseListeners();
	}
	public void mouseClicked(MouseEvent e) {
		RandomGenerator rgen = new RandomGenerator();
		int mx = e.getX();
		int my = e.getY();
		if(one) {
			x2 = mx;
			y2 = my;
			GLine line = new GLine(x1,y1,x2,y2);
			line.setColor(rgen.nextColor());
			add(line);
			one = false;
		} else {
			x1 = mx;
			y1 = my;
			one = true;
		}
	}
	public void mouseDown(MouseEvent e) {
		int x1 = e.getX();
		int y1 = e.getY();
	}
	public void mouseReleased(MouseEvent e) {
		int x2 = e.getX();
		int y2 = e.getY();
		draw();
	}
	private void draw() {
		RandomGenerator rgen = new RandomGenerator();
		GLine line = new GLine(x1,y1,x2,y2);
		line.setColor(rgen.nextColor());
		add(line);
	}
}
