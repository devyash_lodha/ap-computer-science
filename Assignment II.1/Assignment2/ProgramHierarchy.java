/*
 * File: ProgramHierarchy.java
 * Name: Devyash Lodha
 * ---------------------------
 * This file is the starter file for the ProgramHierarchy problem.
 */

import acm.graphics.*;
import acm.program.*;
import java.awt.*;

public class ProgramHierarchy extends GraphicsProgram {
	
	
	public void run() {
		int swidth=1280;
		int sheight=800;
		int mx=swidth/2;
		int my=sheight/2;
		setSize(swidth,sheight);
		
		GLabel  prgm=new GLabel("    Program    ");
		GLabel gprgm=new GLabel("GraphicsProgram");
		GLabel cprgm=new GLabel("ConsoleProgram ");
		GLabel dprgm=new GLabel(" DialogProgram ");
		gprgm.setLocation(mx-(gprgm.getWidth()+256), my-(gprgm.getHeight()/2)+64);
		prgm.setLocation(mx-(prgm.getWidth()), my-(prgm.getHeight()/2)-64);
		cprgm.setLocation(mx-(cprgm.getWidth()-256), my-(cprgm.getHeight()/2)+64);
		dprgm.setLocation(mx-(dprgm.getWidth()-8), my-(dprgm.getHeight()/2)+64);
		prgm.setColor(Color.black);
		gprgm.setColor(Color.black);
		cprgm.setColor(Color.black);
		dprgm.setColor(Color.black);
		
		//GRect prgmR=new GRect(prgm.getWidth(),prgm.getHeight(),prgm.getX(),prgm.getY());
		GRect prgmR=new GRect(prgm.getX()-12,prgm.getY()-prgm.getHeight()-16,prgm.getWidth()+32,prgm.getHeight()+32);
		GRect gprgmR=new GRect(gprgm.getX()-16,gprgm.getY()-gprgm.getHeight()-16,gprgm.getWidth()+32,gprgm.getHeight()+32);
		GRect cprgmR=new GRect(cprgm.getX()-16,cprgm.getY()-cprgm.getHeight()-16,cprgm.getWidth()+32,cprgm.getHeight()+32);
		GRect dprgmR=new GRect(dprgm.getX()-16,dprgm.getY()-dprgm.getHeight()-16,dprgm.getWidth()+36,dprgm.getHeight()+32);
		
		prgmR.setColor(Color.red);
		prgmR.setFilled(false);
		gprgmR.setColor(Color.BLUE);
		gprgmR.setFilled(false);
		cprgmR.setColor(Color.BLUE);
		cprgmR.setFilled(false);
		dprgmR.setColor(Color.BLUE);
		dprgmR.setFilled(false);
		
		double x1,x2,y1,y2;
		x1=prgmR.getX()+(prgmR.getWidth()/2);
		y1=prgmR.getY()+prgmR.getHeight();
		x2=gprgmR.getX()+(gprgmR.getWidth()/2);
		//x2=gprgmR.getY()-(gprgmR.getWidth()/2);
		y2=gprgmR.getY();
		GLine one=new GLine(x1,y1,x2,y2);
		y2=cprgmR.getY();
		x2=cprgmR.getX()+(cprgmR.getWidth()/2);
		GLine two=new GLine(x1,y1,x2,y2);
		y2=dprgmR.getY();
		x2=dprgmR.getX()+(dprgmR.getWidth()/2);
		GLine three=new GLine(x1,y1,x2,y2);
		
		one.setColor(Color.green);
		two.setColor(Color.green);
		three.setColor(Color.green);
		add(prgmR);
		add(gprgmR);
		add(cprgmR);
		add(dprgmR);
		add(prgm);
		add(gprgm);
		add(cprgm);
		add(dprgm);
		add(one);
		add(two);
		add(three);
	}
}