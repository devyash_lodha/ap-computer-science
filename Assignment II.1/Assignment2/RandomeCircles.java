import java.awt.Color;
import acm.graphics.*;
import acm.program.*;
import acm.util.*;

public class RandomeCircles extends GraphicsProgram{
	public void run() {
		int sw=640;
		int sh=480;
		int maxr=25;
		setSize(sw,sh);
		RandomGenerator rdgen = new RandomGenerator();
		while(true) {
			for(int i=0;i<10;i++) {
				int cx = rdgen.nextInt(maxr*2, sw-(maxr*2));
				int cy = rdgen.nextInt(maxr*2, sh-(maxr*2));
				int cr = rdgen.nextInt(5, 50);
				GOval circle = new GOval(cx, cy, cr*2,cr*2);
				boolean filled=rdgen.nextBoolean();
				Color x = rdgen.nextColor();
				circle.setFilled(filled);
				circle.setColor(x);
				add(circle);
			}
		int in = readInt("$Continue? 0=no, otherwise Yes! ");
			if(in==0) {
				break;
			}
		}
	}
}
