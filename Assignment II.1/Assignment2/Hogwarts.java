import acm.program.*;
import acm.util.*;

public class Hogwarts extends ConsoleProgram {
	public void run() {
		RandomGenerator rgen = new RandomGenerator();
		bludger(rgen.nextInt(1000,3000));
	}
	
	private void bludger(int y) {
		int x = y / 1000;
		int z = (x + y);
		x = quaffle(z, y);
		println("Bludger: (" + x + "," + y + "," + z + ")");
	}
	
	private int quaffle(int x, int y) {
		int z = snitch(x + y, y);
		y /= z;
		println("quaffle: (" + x + "," + y + "," + z + ")");
		return z;
	}
	
	private int snitch(int x, int y) {
		y = x / (x % 10);
		println("snitch: (" + x + "," + y + ")");
		return y;
	}
}
