import acm.graphics.*;
import acm.util.*;

public class GHeart extends GCompound {
	public GHeart(double width, double height) {
		RandomGenerator rgen = new RandomGenerator();
		double x = 0;
		double y = 0;
		GArc tleft = new GArc(x,y,width/4,height/4,-270,90);		
		GArc tleftmid = new GArc(x, y, width/4,height/4,90,-90);
		x = tleft.getWidth()+tleftmid.getWidth()-2;
		GArc trightmid = new GArc(x,y,width/4,height/4,-270,90);
		GArc tright = new GArc(x,y,width/4,height/4,90,-90);
		
		x = 0;
		y = height / 4;
		GArc bleft = new GArc(0,(height/4)*-1,width/2,height * 0.75,-90,-90);
		GArc bright = new GArc(0,(height/4)*-1,width/2,height * 0.75,270,90);
		tleft.setColor(rgen.nextColor());
		tleftmid.setColor(rgen.nextColor());
		trightmid.setColor(rgen.nextColor());
		tright.setColor(rgen.nextColor());
		bleft.setColor(rgen.nextColor());
		bright.setColor(rgen.nextColor());
		tleft.setFilled(rgen.nextBoolean());
		tright.setFilled(rgen.nextBoolean());
		tleftmid.setFilled(rgen.nextBoolean());
		trightmid.setFilled(rgen.nextBoolean());
		bleft.setFilled(rgen.nextBoolean());
		bright.setFilled(rgen.nextBoolean());
		tleft.setFillColor(rgen.nextColor());
		tleftmid.setFillColor(rgen.nextColor());
		trightmid.setFillColor(rgen.nextColor());
		tright.setFillColor(rgen.nextColor());
		bleft.setFillColor(rgen.nextColor());
		bright.setFillColor(rgen.nextColor());
		add(tleft);
		add(tleftmid);
		add(trightmid);
		add(tright);
		add(bleft);
		add(bright);
	}
}
