/*
 * File: PythagoreanTheorem.java
 * Name: 
 * Section Leader: 
 * -----------------------------
 * This file is the starter file for the PythagoreanTheorem problem.
 */

import acm.program.*;
import java.math.*;

public class PythagoreanTheorem extends ConsoleProgram {
	public void run() {
		println("root@computer # java PythagoreanTheorem.java");
		while(true){
			println("# Enter A and B to calculate C with the pythagoream theorem!");
			int a=readInt("# a= ");
			int b=readInt("# b= ");
			int as=a*a;
			int bs=b*b;
			int bsqrt=as+bs;
			double ans=Math.sqrt(bsqrt);
			println("# c= "+ans);

			if(readInt("# zero to exit\n# ")==0){
				break;
			}
		}
	}
}
