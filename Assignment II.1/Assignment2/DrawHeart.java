import acm.graphics.*;
import acm.program.*;
import acm.util.*;

public class DrawHeart extends GraphicsProgram {
	public void run() {
		RandomGenerator rgen = new RandomGenerator();
		setSize(500,500);
		for(int i = 0; i < 8000; i++) {
			GHeart h1 = new GHeart(rgen.nextInt(32,300),rgen.nextInt(32,300));
			int x = rgen.nextInt(0,(int) (getWidth() - h1.getWidth()));
			int y = rgen.nextInt(0,(int) (getHeight() - h1.getHeight()));
			h1.setLocation(x,y);
			add(h1);
		}
	}
}