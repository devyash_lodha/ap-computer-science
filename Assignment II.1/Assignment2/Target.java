/*
 * File: Target.java
 * Name: 
 * Section Leader: 
 * -----------------
 * This file is the starter file for the Target problem.
 */

import acm.graphics.*;
import acm.program.*;
import java.awt.*;

public class Target extends GraphicsProgram {	
	public void run() {
		int swidth=1280;
		int sheight=800;
		int mx=swidth/2;
		int my=sheight/2;
		int innerc=64;
		int midc=128;
		int outc=180;
		setSize(swidth,sheight);
		
		GOval out=new GOval(mx-(outc/2),my-(outc/2),outc,outc);
		GOval mid=new GOval(mx-(midc/2),my-(midc/2),midc,midc);
		GOval center=new GOval(mx-(innerc/2),my-(innerc/2),innerc,innerc);
		out.setFilled(true);
		out.setColor(Color.red);
		mid.setFilled(true);
		mid.setColor(Color.white);
		center.setFilled(true);
		center.setColor(Color.red);
		add(out);
		add(mid);
		add(center);
	}
}

