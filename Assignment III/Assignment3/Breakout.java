/*
 * File: Breakout.java
 * -------------------
 * Name: Devyash Lodha
 * Section Leader: 
 * 
 * This file will eventually implement the game of Breakout.
 */

import acm.graphics.*;
import acm.program.*;
import acm.util.*;

import java.applet.*;
import java.awt.*;
import java.awt.event.*;

public class Breakout extends funcs {

/** Width and height of application window in pixels */
	public static final int APPLICATION_WIDTH = 800;
	public static final int APPLICATION_HEIGHT = 600;

/** Dimensions of game board (usually the same) */
	private static final int WIDTH = APPLICATION_WIDTH;
	private static final int HEIGHT = APPLICATION_HEIGHT;

/** Dimensions of the paddle */
	private static final int PADDLE_WIDTH = 80;
	private static final int PADDLE_HEIGHT = 10;

/** Offset of the paddle up from the bottom */
	private static final int PADDLE_Y_OFFSET = 30;

/** Number of bricks per row */
	private static final int NBRICKS_PER_ROW = 5;

/** Number of rows of bricks */
	private static final int NBRICK_ROWS = 2;

/** Separation between bricks */
	private static final int BRICK_SEP = 4;

/** Width of a brick */
	private static final int BRICK_WIDTH =
	  (WIDTH - (NBRICKS_PER_ROW - 1) * BRICK_SEP) / NBRICKS_PER_ROW;

/** Height of a brick */
	private static final int BRICK_HEIGHT = 12;

/** Radius of the ball in pixels */
	private static final int BALL_RADIUS = 10;

/** Offset of the top brick row from the top */
	private static final int BRICK_Y_OFFSET = 70;

/** Number of turns */
	private static final int NTURNS = 3;
	
	private static final double startSpeed = 6.0;

	//the margin between each brick, for the bricks processor
	int brickrow = BRICK_WIDTH + BRICK_SEP;
	//width of a single brick
	int bricksx = APPLICATION_WIDTH / brickrow;
	//margin between each brick for the bricks processor
	int brickheight = BRICK_HEIGHT + BRICK_SEP;
	//speed vars
	double vx,vy;
	//delay between each iteration
	double delay = 25;
	double del = delay;
	//total num of bricks for counting
	int numBricks = NBRICKS_PER_ROW * NBRICK_ROWS;
	//number of turns left
	int turns = NTURNS;
/* Method: run() */
/** Runs the Breakout program. */
	
	//create paddle
	private GRect paddle;
	//create ball
	private GOval ball;
	//create brick
	private GRect brick;
	//audio includes :D
	AudioClip theEnd = MediaTools.loadAudioClip("GameOver_1_.wav");
	AudioClip pong = MediaTools.loadAudioClip("pong.wav");
	AudioClip poof = MediaTools.loadAudioClip("poof.wav");
	AudioClip hit = MediaTools.loadAudioClip("hit.wav");
	AudioClip marioEnd = MediaTools.loadAudioClip("smb_gameover.wav");
	AudioClip newTurn = MediaTools.loadAudioClip("NewTurn.wav");
	AudioClip startSound = MediaTools.loadAudioClip("mk64_countdown.wav");
	AudioClip goodJob = MediaTools.loadAudioClip("GoodJob.wav");
	//GImage back = new GImage("back.jpg");
	
	//main program
	public void run() {
		set();
		play();
		return;
	}
	private void set() {
		RandomGenerator rgen = new RandomGenerator();
//		setBackground(rgen.nextColor());
		//back.setSize(APPLICATION_WIDTH,APPLICATION_HEIGHT);
		//back.setLocation(0,0);
		//add(back);
		createBricks();
		setSize(APPLICATION_WIDTH,APPLICATION_HEIGHT);
		createPaddle();
		createBall();
		vy = startSpeed;
		vx = rgen.nextDouble(1.0,3.0);
		if(rgen.nextBoolean(0.5)) {
			vx = -vx;
		}
	}
	private void newTurn() {
		newTurn.play();
		pause(11000);
		startSound.play();
		RandomGenerator rgen = new RandomGenerator();
		createPaddle();
		createBall();
		delay = 25;
		vy = startSpeed;
		vx = rgen.nextDouble(1.0,3.0);
		if(rgen.nextBoolean(0.5)) {
			vx = -vx;
		}
		play();
	}
	private void play() {
		RandomGenerator rgen = new RandomGenerator();
		startSound.play();
		pause(5100);
		while(true) {
			moveBall();
			pause(delay);
			if(checkCollision()) {
				//gameOver();
				break;
			}
			mouseMove();
			if(turns == 0) {
				gameOver();
			}
			if(numBricks == 0) {
				GLabel GoodJob = new GLabel("You won!");
				GoodJob.setLocation((GoodJob.getWidth() / 2) + (APPLICATION_WIDTH / 2), APPLICATION_HEIGHT / 2);
				GoodJob.setColor(rgen.nextColor());
				add(GoodJob);
				goodJob.play();
				return;
			}
		}
		//theEnd.play();
		pause(30000);
		return;
	}
	private void gameOver() {
		RandomGenerator rgen = new RandomGenerator();
		remove(paddle);
		remove(ball);
		GLabel end = new GLabel("Sorry! You Lost :P");
		end.setLocation(APPLICATION_WIDTH / 2 - (end.getWidth() / 2), APPLICATION_HEIGHT / 2 - (end.getHeight() / 2));
		end.setColor(rgen.nextColor());
		add(end);
		marioEnd.play();
		pause(3000);
	}
	
	//paddlework
	private void createPaddle() {
		RandomGenerator rgen = new RandomGenerator();
		double paddleY = APPLICATION_HEIGHT - PADDLE_Y_OFFSET;
		double paddleX = (APPLICATION_WIDTH / 2) - (PADDLE_WIDTH / 2);
		paddle = new GRect(paddleX, paddleY, PADDLE_WIDTH,PADDLE_HEIGHT);
		paddle.setColor(rgen.nextColor());
		paddle.setFilled(rgen.nextBoolean());
		add(paddle);
	}
	public void mouseMoved(MouseEvent e) {
		paddle.setLocation(e.getX() - (PADDLE_WIDTH / 2), APPLICATION_HEIGHT - PADDLE_Y_OFFSET);
	}
	private void mouseMove() {
		addMouseListeners();
	}
	
	//ballwork
	private void createBall() {
		ball = new GOval((APPLICATION_WIDTH/2) - BALL_RADIUS,(APPLICATION_HEIGHT/2) - BALL_RADIUS,BALL_RADIUS*2,BALL_RADIUS*2);
		RandomGenerator rgen = new RandomGenerator();
		ball.setColor(rgen.nextColor());
		ball.setFilled(rgen.nextBoolean(0.5));
		add(ball);
	}
	private void moveBall() {
		ball.move(vx, vy);
	}
	private boolean checkCollision() {
		RandomGenerator rgen = new RandomGenerator();
		if(ball.getX() >= APPLICATION_WIDTH - (BALL_RADIUS*2) || ball.getX() <= 0 + (BALL_RADIUS*2)) {
			vx = -vx;
			pong.play();
			pause(100);
		}
		if(ball.getY() <= 0) {
			ball.setLocation(ball.getX(), BALL_RADIUS * 2);
			vy = -vy;
			pong.play();
			pause(100);
		}
		getCollidingObject();

		if(ball.getY() >= APPLICATION_HEIGHT - (BALL_RADIUS * 2)) {
			vx = 0;
			vy = 0;
			turns--;
			if(turns!=0) {
				remove(ball);
				remove(paddle);
				newTurn();
			}
			return true;
		} else {
			return false;
		}	
	}
	private void getCollidingObject() {		
		double ballTL_x = ball.getX();
		double ballTL_y = ball.getY();
		double ballBL_x = ball.getX() + (BALL_RADIUS*2);
		double ballBL_y = ball.getY() + (BALL_RADIUS*2);
		double ballTR_x = ball.getX() + (BALL_RADIUS*2);
		double ballTR_y = ball.getY();
		double ballBR_x = ball.getX() + (BALL_RADIUS*2);
		double ballBR_y = ball.getY() + (BALL_RADIUS*2);
		
		//paddle
		if(getElementAt(ballBL_x,ballBL_y) == paddle || getElementAt(ballBR_x,ballBR_y) == paddle) {
			increaseSpeed();
			vy = -vy;
			hit.play();
			pause(100);
			return;
		}
		
		if(getElementAt(ballBL_x,ballBL_y) != paddle && getElementAt(ballBL_x,ballBL_y) != null/*  && getElementAt(ballBL_x,ballBL_y) != back*/) {
			vy = -vy;
		} else if(getElementAt(ballBR_x,ballBR_y) != paddle && getElementAt(ballBR_x,ballBR_y) != null /* && getElementAt(ballBR_x,ballBR_y) != back*/) {
			vy = -vy;
		} else if(getElementAt(ballTL_x,ballTL_y) != paddle && getElementAt(ballTL_x,ballTL_y)!= null /* && getElementAt(ballTL_x,ballTL_y) != back*/) {
			vy = -vy;
		} else if(getElementAt(ballTR_x,ballTR_y) != paddle && getElementAt(ballTR_x,ballTR_y)!= null /*&& getElementAt(ballTR_x,ballTR_y) != back*/) {
			vy = -vy;
		}
		if(breakBrick(ballBL_x,ballBL_y)) {
			return;
		} else {
			if(breakBrick(ballBR_x,ballBR_y))  {
				poof.play();
				pause(100);
				return;
			} else {
				if(breakBrick(ballTL_x,ballTL_y)) {
					poof.play();
					pause(100);
					return;
				} else {
					if(breakBrick(ballTR_x,ballTR_y)) {
						poof.play();
						pause(100);
						return;
					}
				}
			}
		}
	}
	private boolean breakBrick(double x, double y) {
		if(getElementAt(x,y) != null && getElementAt(x,y) != paddle/* && getElementAt(x,y) != back*/) {
			GObject brickID = getElementAt(x,y);
			remove(brickID);
			numBricks--;
			return true;
		} else {
			return false;
		}
	}
	private void increaseSpeed() {
		vy = ((100 - vy)*.1);
	}
	
	//brickwork
	private void createBricks() {
		int height = 0;
		int margin = 0;
		int row = 1;
		for(int a = 0; a < NBRICK_ROWS; a++) {
			margin = 0;
			for(int b = 0; b < bricksx; b++) {
				newBrick(margin,height + BRICK_Y_OFFSET,getColor(row));
				margin += brickrow;
			}
			height+= brickheight;
			row++;
		}
	}
	
	private void newBrick(double x, double y, Color color) {
		brick = new GRect(x,y,BRICK_WIDTH,BRICK_HEIGHT);
		RandomGenerator rgen = new RandomGenerator();
		brick.setColor(color);
		brick.setFilled(true);
		add(brick);
	}
	private Color getColor(int row) {
		Color x = Color.BLACK;
		RandomGenerator rgen = new RandomGenerator();
		if(row == 1 || row == 2) {
			x = Color.RED;
		} else if(row == 3 || row == 4) {
			x = Color.ORANGE;
		} else if(row == 5 || row == 6) {
			x = Color.YELLOW;
		} else if(row == 7 || row == 8) {
			x = Color.GREEN;
		} else if(row == 9 || row == 10) {
			x = Color.CYAN;
		} else {
			x = rgen.nextColor();
		}
		return x;
	}
}
