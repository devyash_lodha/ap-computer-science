import acm.program.*;
import acm.util.*;

public class dateString extends stringfuncs {
	public void run() {
		while(true) {
			int d = readInt("# Day   ==>:");
			int m = readInt("# Month ==>:");
			int y = readInt("# Year  ==>:");
			println("# " + dateString(d,m,y));
		}
	}
}
