import acm.program.*;
import acm.util.*;

public class randomWord_dl extends ConsoleProgram {
	RandomGenerator rgen = new RandomGenerator();
	private static final int MIN_LETTERS = 3;
	private static final int MAX_LETTERS = 8;
	public void run() {
		for(int x = 0; x < 5; x++) {
			String word = "";
			for(int i = 0; i < rgen.nextInt(MIN_LETTERS, MAX_LETTERS); i++) {
				char c = dictionary(rgen.nextInt(0,25));
				word = word + c;
			}
			println("# " + word);
		}
	}
	public boolean isChar(char in) {
		String index = "abcdefghijklmnopqrstuvwxyz";
		boolean consonant = false;
		for(int x = 0; x < index.length(); x++) {
			if(in == index.charAt(x)) {
				consonant = true;
			}
		}
		if(consonant) {
			return true;
		} else {
			return false;
		}
	}
	public char dictionary(int index) {
		String dict = "abcdefghijklmnopqrstuvwxyz";
		char x = dict.charAt(index);
		return x;
	}
}
