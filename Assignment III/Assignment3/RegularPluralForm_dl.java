import acm.program.*;
import acm.util.*;

public class RegularPluralForm_dl extends stringfuncs {
	public void run() {
		setSize(640, 480);
		while(true) {
			String input = readLine("# Please enter a word! \"1234567890\" to exit!\n#\r==>:");
			if(input.equalsIgnoreCase("1234567890")) {
				break;
			}
			String output = "";
			if(input.length() != 1) {
				char lastLetter = input.charAt(input.length() - 1);
				char lastLastLetter = input.charAt(input.length() - 2);
				if(lastLetter == 's' || lastLetter == 'z' || lastLetter == 'z' || lastLetter == 'x') {
					output = input + "es";
				} else if(lastLetter == 'h' || lastLetter == 's') {
					if(lastLastLetter == 's' || lastLastLetter == 'e') {
						output = input + "es";
					}
				} else if(lastLetter == 'y' && isInDictionary(input.charAt(input.length() - 2), "bcdfghjklmnpqrstvwxzBCDFGHJKLMNPQRSTVWXZ")) {
					String out = "";
					for(int i = 0; i < input.length() - 1; i++) {
						out = out + input.charAt(i);
					}
					out = out + "ies";
					output = out;
				} else {
					output = input + "s";
				}
				println("# Output:\n#\t==>:" + output);
			} else {
				println("# Only one letter! Sorry!");
			}
		}
	}
}
