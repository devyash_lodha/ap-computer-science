import acm.program.*;
import acm.util.*;

/*
 * @author Devyash Lodha
 * @summary Scrabble program
 */

public class Scrabble extends stringfuncs {
	private static final String one = "aeilnorstuAEILNORSTU";
	private static final String two = "dgDG";
	private static final String three = "bcmpBCMP";
	private static final String four = "fhvwyFHVWY";
	private static final String five = "kK";
	private static final String eight = "jxJX";
	private static final String ten = "qzQZ";
	
	public void run() {
		setSize(640,480);
		println("# Welcome to Scrabble!");
		int score = 0;
		String input = "";
		while(true) {
			int wordScore = 0;
			input = readLine("# Please enter a word! \"@exit\" to exit!\n#\t==>:");
			if(input.equalsIgnoreCase("@exit")) {
				break;
			}
			for(int i = 0; i < input.length(); i++) {
				if(isInDictionary(input.charAt(i), one)) {
					wordScore+=1;
					score+=1;
				} else if(isInDictionary(input.charAt(i), two)) {
					wordScore+=2;
					score+=2;
				} else if(isInDictionary(input.charAt(i), three)) {
					wordScore+=3;
					score+=3;
				} else if(isInDictionary(input.charAt(i), four)) {
					wordScore+=4;
					score+=4;
				} else if(isInDictionary(input.charAt(i), five)) {
					wordScore+=5;
					score+=5;
				} else if(isInDictionary(input.charAt(i), eight)) {
					wordScore+=8;
					score+=8;
				} else if(isInDictionary(input.charAt(i), ten)) {
					wordScore+=10;
					score+=10;
				}
			}
			println("# The word, " + input + " yields " + wordScore + " Points!\n#   You now have a total of " + score + " Points!");
		}
		println("# GAME OVER! You end with " + score + " Points!");
		return;
	}
}
