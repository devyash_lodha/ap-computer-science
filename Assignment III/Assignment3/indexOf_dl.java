import acm.program.*;
import acm.util.*;

public class indexOf_dl extends stringfuncs {
	public void run() {
		setSize(640,480);
		while(true) {
			String input = readLine("# Please enter a word!\n#\t==>:");
			String key = readLine("# Please enter the search key\n#\t==>:");
			if(key.length() > 1) {
				println("# Key longer than 1 character. Truncating to one character");
			}
			int z = findChar(input, key.charAt(0));
			if(z != -1) {
				println("# Found the key at position " + z + "!");
			} else {
				println("Could not find key!");
			}
		}
	}
}
