import acm.program.*;
import acm.util.*;

public class isConsonant extends ConsoleProgram {
	public void run() {
		String index = "bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ";
		setSize(640,480);
		String in = readLine("# Please enter a character, \"!\" to exit:\n#\t==>:");
		while(in.charAt(0) != '!') {
			boolean consonant = false;
			for(int x = 0; x < index.length(); x++) {
				if(in.charAt(0) == index.charAt(x)) {
					consonant = true;
					println("# " + in.charAt(0) + " is a consonant!");
				}
			}
			if(!consonant) {
				println("$ " + in.charAt(0) + " is not a consonant!");
			}
			in = readLine("# Please enter a character, \"!\" to exit:\n#\t==>:");
		}
		println("# \"!\" pressed! Exiting!");
	}
}
