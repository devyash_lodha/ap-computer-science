import acm.program.*;
import acm.util.*;

//	Author: Devyash Lodha
//	Class: AP Computer Science Block 4
//	Program: capitalize.java
//	summary: capitalizes the first letter of the string and makes everything else lower case!
//	Iterates through the entire string and makes the string lowercase. It decapitates the first letter and replaces it with it's  capital equivalent

public class capitalize extends ConsoleProgram {
	public void run() {
		char x = 'd';
		while(x != '!') {
			setSize(640,480);
			String str = readLine("# Please enter a string! \"!\" to exit!\n#\t==>:");
			char last = str.charAt(0);
			String letterone = "";
			String word = "";
			letterone = letterone + last;
			letterone = letterone.toUpperCase();
			str.toLowerCase();
			for(int i = 1; i < str.length(); i++) {
				x = str.charAt(i);
				String otherLetter = "";
				otherLetter = otherLetter + x;
				otherLetter = otherLetter.toLowerCase();
				otherLetter = otherLetter.trim();
				word = word + otherLetter;
			}
			x = letterone.charAt(0);
			String out = x + word;
			println("# The String is\n#\t==>:" + out);
		}
		println("# \"!\" Was Pressed! Now Exiting!");
		return;
	}
}
