/*
 * File: StoneMasonKarel.java
 * --------------------------
 * The StoneMasonKarel subclass as it appears here does nothing.
 * When you finish writing it, it should solve the "repair the quad"
 * problem from Assignment 1.  In addition to editing the program,
 * you should be sure to edit this comment so that it no longer
 * indicates that the program does nothing.
 */



/*
	run() just runs everyhting over and over until karel is done
	moveDist() takes an integer and goes the distance specified. It also doesn't execute the move() command if there is something in the way
	PickBeeper2() picks a beeper if there is one to pick
	placeBeeper2() places a beeper if there ain't one already
	exec() Main routine, executed over and over.
		if the front is clear, go ahead and destroy what is there already and build it again.
		Do this once again to prevent a fencepost error
*/

import stanford.karel.*;

public class StoneMasonKarel extends SuperKarel {
	public void run() {
		while( frontIsClear() ) {
			exec();
		}
		exec();
	}
	
	private void moveDist( int distance ) {
		for( int i = 0; i < distance; i++ ) {
			if( frontIsClear() ) {
				move();
			}
		}
	}
	
	private void pickBeeper2() {
		if( beepersPresent() ) {
			pickBeeper();
		}
	}
	
	private void placeBeeper2() {
		if( !beepersPresent() ) {
			putBeeper();
		}
	}
	
	private void exec() {
		turnLeft();
		while( frontIsClear() ) {
			pickBeeper2();
			moveDist(1);
		}
		pickBeeper2();
		turnAround();
		placeBeeper2();
		while( frontIsClear() ) {
			placeBeeper2();
			moveDist(1);
		}
		placeBeeper2();
		turnLeft();
		moveDist(4);

	}
}
