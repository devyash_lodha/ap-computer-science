/*
 * File: CollectNewspaperKarel.java
 * --------------------------------
 * At present, the CollectNewspaperKarel subclass does nothing.
 * Your job in the assignment is to add the necessary code to
 * instruct Karel to walk to the door of its house, pick up the
 * newspaper (represented by a beeper, of course), and then return
 * to its initial position in the upper left corner of the house.
 */

import stanford.karel.*;

public class CollectNewspaperKarel extends SuperKarel {

	public void run() {
		turnRight();//right
		moveDist(1);//move 1
		turnLeft();//left
		moveDist(3);//move 3
		pickBeeper();//pick newspaper
		turnAround();//turn around
		moveDist(3);//move 3
		turnRight();//turn right
		moveDist(1);//move once
		turnRight();//turn right
	}
	
	private void moveDist( int x ) {										//move int x times
		for( int counter = 0; counter < x; counter++ ) {					//performs move int x times
			if( frontIsClear() ) {											//is the front clear
				move();														//move
			}
		}
	}
}
