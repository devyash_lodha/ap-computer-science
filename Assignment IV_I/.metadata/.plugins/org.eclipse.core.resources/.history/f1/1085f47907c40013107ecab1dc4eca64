/*
 * File: HangmanCanvas.java
 * ------------------------
 * This file keeps track of the Hangman display.
 */

import acm.graphics.*;

public class HangmanCanvas extends GCanvas {

/** Resets the display so that only the scaffold appears */
	private GLabel dispWord;
	private GLabel incGuesses;
	private GLabel turns;
	public void reset() {
		removeAll();
		GLine floor = new GLine(32, 480-32, 320 - 32, 480-32);
		add(floor);
		GLine backbone = new GLine(64, 480-32, 64, 480-32-SCAFFOLD_HEIGHT);
		add(backbone);
		GLine roof = new GLine(64, 480-32-SCAFFOLD_HEIGHT, 64+BEAM_LENGTH, 480-32-SCAFFOLD_HEIGHT);
		add(roof);
		GLine rope = new GLine(64+BEAM_LENGTH, 480-32-SCAFFOLD_HEIGHT, 64+BEAM_LENGTH, 480-32-SCAFFOLD_HEIGHT+ROPE_LENGTH);
		add(rope);
		dispWord = new GLabel("Word: ", 32, 40);
		add(dispWord);
		incGuesses = new GLabel("Incorrect Guesses: ", 32, 20);
		add(incGuesses);
		//turns = new GLabel("Turns Left: ", 32, 60);
		//add(turns);
	}

/**
 * Updates the word on the screen to correspond to the current
 * state of the game.  The argument string shows what letters have
 * been guessed so far; unguessed letters are indicated by hyphens.
 */
	public void displayWord(String word) {
		dispWord.setLabel("Word: " + word);
	}

/**
 * Updates the display to correspond to an incorrect guess by the
 * user.  Calling this method causes the next body part to appear
 * on the scaffold and adds the letter to the list of incorrect
 * guesses that appears at the bottom of the window.
 */
	public void noteIncorrectGuess(String guesses) {
		incGuesses.setLabel("Incorrect Guesses: " + guesses);
	}
	
	public void updateSkeleton(int num)
	{
		if(num == 1)
		{
			add(new GOval(64 + BEAM_LENGTH - 18, 480-32-SCAFFOLD_HEIGHT+ROPE_LENGTH, 36, 36));
		} else {
			if(num == 2)
			{
				add(new GLine(64+BEAM_LENGTH, 480-32-SCAFFOLD_HEIGHT+ROPE_LENGTH+HEAD_RADIUS, 64+BEAM_LENGTH, 480-32-SCAFFOLD_HEIGHT+ROPE_LENGTH+HEAD_RADIUS+BODY_LENGTH));
			} else {
				if(num == 3)
				{
					add(new GLine(64+BEAM_LENGTH, 480-32-SCAFFOLD_HEIGHT+ROPE_LENGTH+ARM_OFFSET_FROM_HEAD, 64+BEAM_LENGTH-64, 480-32-SCAFFOLD_HEIGHT+ROPE_LENGTH+ARM_OFFSET_FROM_HEAD));
				}
			}
		}
	}
/* Constants for the simple version of the picture (in pixels) */
	private static final int SCAFFOLD_HEIGHT = 360;
	private static final int BEAM_LENGTH = 144;
	private static final int ROPE_LENGTH = 18;
	private static final int HEAD_RADIUS = 36;
	private static final int BODY_LENGTH = 144;
	private static final int ARM_OFFSET_FROM_HEAD = 28;
	private static final int UPPER_ARM_LENGTH = 72;
	private static final int LOWER_ARM_LENGTH = 44;
	private static final int HIP_WIDTH = 36;
	private static final int LEG_LENGTH = 108;
	private static final int FOOT_LENGTH = 28;

}
