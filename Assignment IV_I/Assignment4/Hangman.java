/*
 * File: Hangman.java
 * ------------------
 * This is Devyash's main class for Hangman!
 */

import acm.graphics.*;
import acm.program.*;
import acm.util.*;

public class Hangman extends ConsoleProgram
{
	private HangmanLexicon words = new HangmanLexicon();
	private HangmanCanvas canvas = new HangmanCanvas();
	
	private int guesses = 8;
	private String word;
	private String hiddenWord;
	private char guess;
	private String incorrect = "";
	
	public void init()
	{
		setSize(640, 480);
		canvas.reset();
		add(canvas);
	}
	
	public void run()
	{
		if(words.generateLexicon("HangmanLexicon.txt"))
		{
			println(words.getWordCount());
			while(true)
			{
				word = words.getWord();
				hiddenWord = dashes();
				setup();
				play();
				readLine("Enter to restart!");
			}
		} else {
			println("Lexicon not found! Exiting!");
		}
	}
	
	private void setup()
	{
		println("Welcome!");
		println("The word is " + word);
		println("The word is " + hiddenWord);
		println("You have " + guesses + " guesses left");
	}
	
	private String dashes()
	{
		String output = "";
		for(int i = 0; i < word.length(); i++)
		{
			output = output + "-";
		}
		return output;
	}
	
	private void play()
	{
		while(guesses > 0)
		{
			String input = readLine("Guess please: ");
			while(true)
			{
				if(input.length() > 1)
				{
					input = readLine("Only one character at a time: ");
				} else {
					if(input.length() ==1)
					{
						break;
					}
				}
			}
			guess = input.charAt(0);
			guess = Character.toUpperCase(guess);
			checkGuess();
			if(hiddenWord.equals(word))
			{
				println("Yay! You won! The word is: " + word);
			}
			println("The word is now: " + hiddenWord);
			canvas.displayWord(hiddenWord);
			println("You have " + guesses + " guesses left!");
			canvas.updateGuesses(guesses);
			if(hiddenWord.equals(word))
			{
				break;
			}
			canvas.updateSkeleton(8-guesses);
			if(guesses == 0)
			{
				println("You just lost! Sorry! Bad Luck :(. At least I won!");
				println("The word was actually: " +  word);
				println("Qua Qua Qua!!!");
			}
		}
	}
	
	private void checkGuess()
	{
		if(word.indexOf(guess) == -1)
		{
			println("Sorry! Guess wrong! No " + guess + "'s in the word!");
			incorrect = incorrect + guess;
			canvas.noteIncorrectGuess(incorrect);
			guesses--;
		} else {
			println("The guess was correct!");
		}
		
		for(int i = 0; i < word.length(); i++)
		{
			if(guess == word.charAt(i))
			{
				if(i > 0)
				{
					hiddenWord = hiddenWord.substring(0, i) + guess + hiddenWord.substring(i + 1);
				} else {
					if(i == 0)
					{
						hiddenWord = guess + hiddenWord.substring(1);
					}
				}
			}
		}
	}
}
