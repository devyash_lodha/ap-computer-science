import acm.program.*;
import acm.util.*;
import acm.graphics.*;

public class dl_ImageFunctions extends GraphicsProgram
{
	public static final int CV_RGB2GRAY = 0;
	public static final int CV_FLIP_HORIZONTAL = 0;
	public static final int CV_FLIP_VERTICAL = 1;
	
	public GImage cvtColor(GImage input, int convertCode)
	{
		if(convertCode == CV_RGB2GRAY)
		{
			//retrieve pixel array
			int[][] image = input.getPixelArray();
			//go through the entire image
			for(int a = 0; a < image.length; a++)
			{
				for(int b = 0; b < image[a].length; b++)
				{
					//extract the color values
					int pixel = image[a][b];
					int red = (pixel >> 16) & 0xFF;
					int green = (pixel >> 8) & 0xFF;
					int blue = pixel & 0xFF;
					//calculate the luminosity
					int luminosity = calculateLuminosity(red, green, blue);
					//encode new pixel value
					pixel = (0xFF << 24) | (luminosity << 16) | (luminosity << 8) | luminosity;
					//set the pixel value to grayscale
					image[a][b] = pixel;
				}
			}
			//return the image
			return new GImage(image);
		} else {
			return null;
		}
	}
	
	//calculate the luminosity
	public int calculateLuminosity(int red, int green, int blue)
	{
		return GMath.round(0.299*red+0.587*green+0.114*blue);
	}
	
	//flip the image
	public GImage flip(GImage input, int code)
	{
		int[][] image = input.getPixelArray();
		int height = image.length;
		if(code == CV_FLIP_VERTICAL)
		{
			for(int a = 0; a < height / 2; a++)
			{
				int b = height - a - 1;
				int[] t = image[a];
				image[a] = image[b];
				image[b] = t;
			}
			return new GImage(image);
		}
		return null;
	}
}