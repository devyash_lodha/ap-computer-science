import acm.program.*;
import acm.util.*;
import java.io.*;
import java.util.*;

//UniqueNames (Devyash Lodha)

public class dl_UniqueNames extends ConsoleProgram
{
	//stores the names
	Vector<String> names = new Vector<String>(1,1);
	public void run()
	{
		//ask for the names
		ask();
		//print the results
		printOut();
	}
	
	private void printOut()
	{
		//print out the entire list of names
		println("The list contains:");
		for(int i = 0; i < names.size(); i++)
		{
			println(names.get(i));
		}
	}
	
	private void ask()
	{
		//infinite loop
		while(true)
		{
			//input the name
			String input = readLine("Please enter the name: ");
			//if the name is empty, we are done
			if(input.equals(""))
			{
				break;
			}
			//add the name to the Vector
			if(!names.contains(input))
			{
				names.add(input);
			} else {
				//if the person has already entered that, reject it!
				println("You already entered that!");
			}
		}
	}
}
