import acm.program.*;
import acm.util.*;

public class SieveOfErastothenes extends ConsoleProgram
{
	//the upper value, to end at
	int high = 1000;
	public void run()
	{
		//set the screen size
		setSize(640, 480);
		//loop indefinitely
		while(true)
		{
			//ask what the user want's to do
			println("> What do you want to do?\n> exit = \"@exit\"\n> start = anything else");
			String command = readLine("> Your command please\n< ");
			//if the user wants to exit, end the program
			if(command.equalsIgnoreCase("@exit"))
			{
				println("> Exiting!");
				break;
			} else {
				high = 0;
				while(high < 2)
				{
					//gets the upper value
					high = readInt("> Please enter an upper value!\n< ");
				}
				//this stores which numbers are crossed
				boolean[] done = new boolean[high+1];
				//go through the numbers, and do the calculations
				for(int i = 2; i <= high; i++)
				{
					//if it is not crossed out, cross out all multiples
					if(!done[i])
					{
						//print the number
						print(i + ", ");
						//cross the numbers out and set the status to true
						for(int j = i; j <= high; j+=i)
						{
							done[j] = true;
						}
					}
				}
				print("\n");
			}
		}
	}
}