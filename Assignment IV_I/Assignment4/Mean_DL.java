import acm.program.*;
import acm.util.*;
import java.util.*;

public class Mean_DL extends ConsoleProgram {
	AdvancedFunctions kit = new AdvancedFunctions();
	Vector<Double> nums = new Vector<Double>(7, 1);
	public void run() {
		setSize(640, 480);
		while(true) {
			//get the numbers
			for(int z = 0; z < 7; z++) {
				println("# Please enter a number!");
				nums.add(readDouble("#\t==>:"));
				double sum = 0;
				for(int i = 0; i < nums.size(); i++) {
					sum+= nums.elementAt(i);
				}
				//calculate the average
				double avg = sum / nums.size();
				println("# Average: " + avg);
			}
			println("END!");
		}
	}
}
