import acm.program.*;
import acm.util.*;

public class AdvancedFunctions extends ConsoleProgram {
	public static final int STRING_LOWERCASE = 0;
	public static final int STRING_UPPERCASE = 1;
	public static final int STRING_LETTERS = 2;
	public static final int STRING_VOWELS = 3;
	public static final int STRING_CONSONANTS = 4;
	private static final String lowercase = "abcdefghijklmnopqrstuvwxyz";
	private static final String uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static final String letters = lowercase + uppercase;
	private static final String vowels = "aeiouyAEIOUY";
	private static final String consonants = "bcdfghjklmnpqrstvwxzBCDFGHJKLMNPQRSTVWXZ";
		
	public boolean isInDictionary(char letter, String dictionary) {
		for(int i = 0; i < dictionary.length(); i++) {
			if(letter == dictionary.charAt(i)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isInDictionary(char letter, int dictionaryID) {
		return isInDictionary(letter, getDictionary(dictionaryID));
	}
	
	public char charToUppercase(char character) {
		String work = "";
		work = work + character;
		work.toUpperCase();
		return work.charAt(0);
	}
	
	public char charToLowercase(char character) {
		String work = "";
		work = work + character;
		work.toLowerCase();
		return work.charAt(0);
	}
	
	public String getDictionary(int dictionaryID) {
		if(dictionaryID == 0) {
			return lowercase;
		} else if(dictionaryID == 1) {
			return uppercase;
		} else if(dictionaryID == 2) {
			return letters;
		} else if(dictionaryID == 3) {
			return vowels;
		} else if(dictionaryID == 4) {
			return consonants;
		} else {
			return null;
		}
	}
	
	public char randomCharacter(String dictionary) {
		RandomGenerator rgen = new RandomGenerator();
		int letter = rgen.nextInt(0, dictionary.length());
		return dictionary.charAt(letter);
	}
	
	public char randomCharacter(int dictionaryID) {
		return randomCharacter(getDictionary(dictionaryID));
	}
	
	public String dateString(int day, int month, int year) {
		String m = "";
		String output = "";
		if(month == 1) {
			m = "Jan";
		} else if(month == 2) {
			m = "Feb";
		} else if(month == 3) {
			m = "Mar";
		} else if(month == 4) {
			m = "Apr";
		} else if(month == 5) {
			m = "May";
		} else if(month == 6) {
			m = "Jun";
		} else if(month == 7) {
			m = "Jul";
		} else if(month == 8) {
			m = "Aug";
		} else if(month == 9) {
			m = "Sep";
		} else if(month == 10) {
			m = "Oct";
		} else if(month == 11) {
			m = "Nov";
		} else if(month == 12) {
			m = "Dec";
		} else {
			m = "Invalid Month!";
		}
		output = day + "-" + m + "-";
		String yr = "";
		yr = yr + year;
		if(yr.length() == 2) {
			output = output + yr;
		} else {
			output = output + yr.charAt(2) + yr.charAt(3);
		}
		return output;
	}
	public int findChar(String input, char key) {
		for(int i = 0; i < input.length(); i++) {
			if(input.charAt(i) == key) {
				return i;
			}
		}
		return -1;
	}
	public String stripChars(String input, String dictionary) {
		String output = "";
		for(int i = 0; i < input.length(); i++) {
			if(isInDictionary(input.charAt(i), dictionary)) {
				output = output + input.charAt(i);
			}
		}
		return output;
	}
	public String stripChars(String input, int dictionary) {
		return stripChars(input, getDictionary(dictionary));
	}
}