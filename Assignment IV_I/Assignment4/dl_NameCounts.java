import acm.program.*;
import acm.util.*;
import java.util.*;

public class dl_NameCounts extends ConsoleProgram
{
	//create the HashMap
	private Map<String, Integer> names = new HashMap<String, Integer>();
	
	//initialise the program by setting the screen size
	public void init()
	{
		setSize(640, 480);
	}
	
	//this is our main method. We read the names, and display the names to the screen
	public void run()
	{
		readNames();
		printNames();
	}
	
	//we need to generate the output
	private void printNames()
	{
		//Create an iterator so we can iterate through each value of the Map
		Iterator<String> i = names.keySet().iterator();
		//keep going until the end of the map
		while(i.hasNext())
		{
			//get the next key
			String key = i.next();
			//store the number of that name as an integer
			int count = names.get(key);
			//display the data
			println("Entry [" + key + "] has count " + count);
		}
	}
	
	//generate the HashMap
	private void readNames()
	{
		//loop forever
		while(true)
		{
			//get the name to store
			String name = readLine("Enter name: ");
			//if the user entered, "", exit
			if(name.equals(""))
			{
				break;
			} else {
				//store the number of the names in the Map in a temporary variable
				Integer x = names.get(name);
				//check to see if x is empty and then say is is one Integer
				if(x == null)
				{
					x = new Integer(1);
					//check to see if x is not empty, then assign it x+1 values
				} else {
					x = new Integer(x+1);
				}
				//put the name into the HashMap
				names.put(name,  x);
			}
		}
	}
}
