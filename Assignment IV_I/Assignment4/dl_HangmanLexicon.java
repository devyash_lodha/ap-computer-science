/*
 * File: HangmanLexicon.java
 * -------------------------
 * This file contains a stub implementation of the HangmanLexicon
 * class that you will reimplement for Part III of the assignment.
 */

import acm.util.*;
import acm.program.*;
import java.io.*;
import java.util.*;

public class dl_HangmanLexicon extends ConsoleProgram {
	//create the RandomGenerator for random numbers
	RandomGenerator rgen = new RandomGenerator();
	//An ArrayList to store all the words within
	private ArrayList<String> wordList = new ArrayList<String>();
	
	//Here, we open the lexicon file and store the values it stores
	public boolean generateLexicon(String location) {
		//This is to prevent those nasty little crashes!
		try {
			//The BufferedReader is what we will use to open the lexicon. We now will open this file
			BufferedReader br = new BufferedReader(new FileReader(location));
			//now, we need to store the values from the BufferedReader file
			while(true) {
				//Temporary String to store the current selected value within
				String w = br.readLine();
				//check to make sure that temporary String is not null. If it is null, that means that the end of the file has
				//been reached and now it is time to exit the loop
				if(w == null) {
					//we must properly close the file to free the resource for other processes to access and to prevent
					//data corruption
					br.close();
					//we will now exit from this function, successfully
					break;
				}
				//We need to add the word stored in that temporary variable back into the RAM lexicon
				wordList.add(w);
			}
			//Everything has been successful, so we send back a value of true
			return true;
			//Handle any exceptions that could have occurred
		} catch (IOException e) {
			//exit out of this function with a failure. We send back the value of false
			return false;
		}
	}
	
	//We need to get the word from the lexicon. This is where we do it!
	public String getWord(int index) {
		//Check to make sure that the index is valid and someone isn't derping up
		if(index < 0 || index > wordList.size()) {
			//Send back a fail statement because we failed to get the word
			return "Sorry! No words available";
		} else {
			//send back the word with the index we were passed
			return wordList.get(index);
		}
	}
	
	//redeclare the above function so we don't need to supply it with an index
	public String getWord() {
		//return the word. We are running the function above, but automatically generating the random numbers
		return getWord(rgen.nextInt(0, getWordCount()));
	}
	
	//get the number of words currently in the lexicon
	public int getWordCount() {
		return wordList.size();
	}
	
	//remove all words from the lexicon
	public void clearDictionary() {
		wordList.clear();
	}
}
