/*
 * File: Hangman.java
 * ------------------
 * This is Devyash's main class for Hangman!
 */

import acm.graphics.*;
import acm.program.*;
import acm.util.*;

//declare the class and state that it extends ConsoleProgram
public class dl_Hangman extends ConsoleProgram
{
	//This is the lexicon of words
	private dl_HangmanLexicon words = new dl_HangmanLexicon();
	//This is the canvas upon which we draw shapes, figures and text
	private dl_HangmanCanvas canvas = new dl_HangmanCanvas();
	
	//the number of guesses
	private int guesses = 8;
	//The current word which we are solving
	private String word;
	//the masked word
	private String hiddenWord;
	//the single character guess
	private char guess;
	//This stores the incorrect characters
	private String incorrect = "";
	
	//This is the first function to run when we start the program
	public void init()
	{
		//reset window size
		setSize(640, 480);
		//clear the canvas and add the basic functions back
		canvas.reset();
		//add the canvas to the screen
		add(canvas);
	}
	
	public void run()
	{
		//generate the Lexicon and make sure it was successful
		if(words.generateLexicon("HangmanLexicon.txt"))
		{
			//print the number of words in the lexicon to the screen [DEBUG]
			println(words.getWordCount());
			//Game loop
			while(true)
			{
				//Assign a new word to the String word
				word = words.getWord();
				//Dashify (mask) that word
				hiddenWord = dashes();
				//print the starting information
				setup();
				//start playing the actual game
				play();
				readLine("Enter to restart!");
				//reset the canvas
				canvas.reset();
				//set the guess counter back to 8
				guesses = 8;
				//clear the incorrect letterbank
				incorrect = "";
			}
			//If the lexicon generation caused an error
		} else {
			//print an error to the screen [ERROR]
			println("Lexicon not found! Exiting!");
			return;
		}
	}
	
	//prints out the starter information to the screen
	private void setup()
	{
		//just print out the setup info
		println("Welcome!");
		println("The word is " + word);
		println("The word is " + hiddenWord);
		println("You have " + guesses + " guesses left");
	}
	
	//masks the word
	private String dashes()
	{
		//clear the temporary variable
		String output = "";
		//iterate for every letter of the word
		for(int i = 0; i < word.length(); i++)
		{
			//add a dash to the word
			output = output + "-";
		}
		//return that mask
		return output;
	}
	
	//Let's now start playing the game
	private void play()
	{
		//loop until the player runs out of guesses
		while(guesses > 0)
		{
			//capture the guess from the player
			String input = readLine("Guess please: ");
			//trim characters from the guess
			input.trim();
			//We need to validate the guess
			while(true)
			{
				//check if the guess is greater than 1 character
				if(input.length() > 1)
				{
					//ask for input again
					input = readLine("Only one character at a time!\nGuess please: ");
				} else {
					//check if the length is 1
					if(input.length() == 1)
					{
						//exit out of this loop
						break;
					}
				}
			}
			//put the first character into a char
			guess = input.charAt(0);
			//make that character uppercase
			guess = Character.toUpperCase(guess);
			//check the guess
			checkGuess();
			//check to see if the user won yet
			if(hiddenWord.equals(word))
			{
				println("Yay! You won! The word is: " + word);
			}
			//update the dashed word
			println("The word is now: " + hiddenWord);
			//display the masked word on the canvas
			canvas.displayWord(hiddenWord);
			//tell the player the number of turns left
			println("You have " + guesses + " guesses left!");
			//update the skeleton in the canvas
			canvas.updateGuesses(guesses);
			//check to see if the user won and then exit
			if(hiddenWord.equals(word))
			{
				break;
			}
			//update the skeleton
			canvas.updateSkeleton(8-guesses);
			//check to see for doom
			if(guesses == 0)
			{
				println("You just lost! Sorry! Bad Luck :(. At least I won!");
				println("The word was actually: " +  word);
				println("Qua Qua Qua!!!");
			}
		}
	}
	
	//we will now check the guess
	private void checkGuess()
	{
		//if the guess is not in the word
		if(word.indexOf(guess) == -1)
		{
			//sorry, sir!
			println("Sorry! Guess wrong! No " + guess + "'s in the word!");
			//add to the incorrect bank
			incorrect = incorrect + guess;
			//update the incorrect guesses
			canvas.noteIncorrectGuess(incorrect);
			guesses--;
		} else {
			//let the user know, HOORAY!!!
			println("The guess was correct!");
		}
		
		//iterate through the length of the word
		for(int i = 0; i < word.length(); i++)
		{
			//check to see if the guess is the selected character
			if(guess == word.charAt(i))
			{
				//if the position is greater than zero, put the beginning and the ending
				if(i > 0)
				{
					hiddenWord = hiddenWord.substring(0, i) + guess + hiddenWord.substring(i + 1);
					//otherwise, just put the letter then the ending
				} else {
					if(i == 0)
					{
						hiddenWord = guess + hiddenWord.substring(1);
					}
				}
			}
		}
	}
}
//end of the program! Time to party!
