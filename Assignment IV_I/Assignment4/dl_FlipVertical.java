import acm.program.*;
import acm.graphics.*;
import acm.util.*;

public class dl_FlipVertical extends GraphicsProgram
{
	private static final String imageLocation = "FunnyFace.jpg";
	public void run()
	{
		//open the image
		GImage unchanged = new GImage(imageLocation);
		//define the processing object
		dl_ImageFunctions cv = new dl_ImageFunctions();
		//RandomGenerator
		RandomGenerator rgen = new RandomGenerator();
		//flip the image
		GImage flipped = cv.flip(unchanged, cv.CV_FLIP_VERTICAL);
		//set the screen size
		setSize(640, 480);
		//set flip to true
		boolean flip = true;
		//infinte loop
		while(true)
		{
			//remove everything from the scren
			removeAll();
			//flip the image
			if(flip)
			{
				flipped.setSize(640, 480);
				flipped.setLocation(0, 0);
				add(flipped);
			} else {
				unchanged.setSize(640, 480);
				unchanged.setLocation(0, 0);
				add(unchanged);
			}
			//invert the flag
			flip = !flip;
			//give time for the screen to update
			pause(rgen.nextInt(10, 100));
		}
	}
}
