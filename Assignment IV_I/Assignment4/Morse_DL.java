import acm.program.*;
import acm.util.*;
import acm.io.*;
import java.util.*;

//Morse code generator (Devyash Lodha)

public class Morse_DL extends ConsoleProgram
{
	//this stores the code
	String[] codes = new String[26];
	//my string functions class
	AdvancedFunctions kit = new AdvancedFunctions();
	public void run()
	{
		//set screen size
		setSize(640, 480);
		//create the sequence of digits
		createSequence();
		//infinite loop
		while(true)
		{
			//welcome the user and tell what to do
			println("# Welcome to the morse code generator!");
			//get the command
			String command = readLine("# Please enter the command\n#\tstart to run\n#\t==>:");
			//parse the command
			if(command.equalsIgnoreCase("start"))
			{
				convert();
			}
		}
	}
	private void convert()
	{
		//get the string to convert to morse
		String input = readLine("# Please enter a string!\n#\t==>:");
		//remove whitespace at the end
		kit.stripChars(input, kit.STRING_LETTERS);
		String output = "";
		//get the code and convert it, letter by letter
		for(int i = 0; i < input.length(); i++)
		{
			output += codes[getCode(input.charAt(i))];
			output += " ";
		}
		//output the results
		println("# Morse:\n#\t" + output);
	}
	
	private int getCode(char x)
	{
		//convert the key code to the numbers in the array
		switch(x)
		{
		case 'a': return 0;
		case 'b': return 1;
		case 'c': return 2;
		case 'd': return 3;
		case 'e': return 4;
		case 'f': return 5;
		case 'g': return 6;
		case 'h': return 7;
		case 'i': return 8;
		case 'j': return 9;
		case 'k': return 10;
		case 'l': return 11;
		case 'm': return 12;
		case 'n': return 13;
		case 'o': return 14;
		case 'p': return 15;
		case 'q': return 16;
		case 'r': return 17;
		case 's': return 18;
		case 't': return 19;
		case 'u': return 20;
		case 'v': return 21;
		case 'w': return 22;
		case 'x': return 23;
		case 'y': return 24;
		case 'z': return 25;
		default : return -1;
		}
	}
	
	//generate the character array
	private void createSequence()
	{
		codes[0] = ".-";
		codes[1] = "-...";
		codes[2] = "-.-.";
		codes[3] = "-..";
		codes[4] = ".";
		codes[5] = "..-.";
		codes[6] = "--.";
		codes[7] = "....";
		codes[8] = "..";
		codes[9] = ".---";
		codes[10] = "-.-";
		codes[11] = ".-..";
		codes[12] = "--";
		codes[13] = "-.";
		codes[14] = "---";
		codes[15] = ".--.";
		codes[16] = "--.-";
		codes[17] = ".-.";
		codes[18] = "...";
		codes[19] = "-";
		codes[20] = "..-";
		codes[21] = "...-";
		codes[22] = ".--";
		codes[23] = "-..-";
		codes[24] = "-.--";
		codes[25] = "--..";
	}
}
