import acm.program.*;
import acm.util.*;
import java.io.*;

//wordCount (Devyash Lodha)

public class dl_WordCount extends ConsoleProgram
{
	public void run()
	{
		//set the window size automatically to make the program less annoying
		setSize(640, 480);
		//number of lines
		int lines = 0;
		//number of words
		int words = 0;
		//number of characters
		int chars = 0;
		
		//to prevent crashes, we detect errors and properly handle them
		try
		{
			//we create the object that will read the file
			BufferedReader fin = openFileReader();
			//loop, keep iterating through file until we reach the end
			while(true)
			{
				//copy over the next line
				String line = fin.readLine();
				//check to see if we have hit the end of the file
				if(line == null)
				{
					break;
				}
				//increment the number of lines, because we just read a line
				lines++;
				//count the words in the line
				words += countWords(line);
				//find the number of characters by calculating the line length
				chars += line.length();
			}
			//detect weird errors
		} catch(IOException e) {
			println("Something awry just happened :(");
		}
		//output the number of lines and words
		println("Lines: " + lines + "\nWords: " + words + "\nChars: " + chars);
	}
	
	//ask the user to enter a file name
	private BufferedReader openFileReader()
	{
		//assign null to the buffered reader
		BufferedReader fin = null;
		//loop while the buffered reader is null
		while(fin == null)
		{
			//ask for the filename
			String name = readLine("Enter file: ");
			//validate that data
			try
			{
				//open that file
				fin = new BufferedReader(new FileReader(name));
			} catch (IOException ex) {
				//if it can't open the file, output error
				println("I cannot open that file!");
			}
		}
		//send back the BufferedReader
		return fin;
	}
	
	//count the words in the line of the file
	private int countWords(String line)
	{
		//is the program in a word or not
		boolean in = false;
		//the number of words in this line
		int words = 0;
		//iterate through the entire line
		for(int i = 0; i < line.length(); i++)
		{
			//convert the selected character in the String to a char
			char letter = line.charAt(i);
			//check to see if it is not whitespace
			if(Character.isLetterOrDigit(letter))
			{
				//set the flag to true because we are within a word
				in = true;
			} else {
				//check to see if we were in a word
				if(in)
				{
					//increment the word counter
					words++;
				}
				//set the flag back to false
				in = false;
			}
		}
		if(in)
		{
			words++;
		}
		return words;
	}
}
