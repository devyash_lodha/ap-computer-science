import acm.program.*;
import acm.util.*;
import acm.graphics.*;

public class dl_GrayScale extends GraphicsProgram
{
	private static final String imageLocation = "FunnyFace.jpg";
	public void run()
	{
		//create the image processing class
		dl_ImageFunctions cv = new dl_ImageFunctions();
		//get image
		GImage input = new GImage(imageLocation);
		//create new image, and convert the old image to grayscale. save the new image here
		GImage grayScale = cv.cvtColor(input, cv.CV_RGB2GRAY);
		//if the pic to show is gray
		boolean gray = false;
		//set the window size
		setSize(640, 480);
		//infinite loop
		while(true)
		{
			//removes everything from the screen
			removeAll();
			//invert gray
			gray = !gray;
			//show the grayscale image
			if(gray)
			{
				grayScale.setLocation(0, 0);
				grayScale.setSize(640, 480);
				add(grayScale);
				//show the regular image
			} else {
				input.setLocation(0, 0);
				input.setSize(640, 480);
				add(input);
			}
			//wait, so the user can see the picture
			pause(1000);
		}
	}
}
