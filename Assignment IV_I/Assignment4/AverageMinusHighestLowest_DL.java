import acm.program.*;
import acm.util.*;
import java.util.*;

//AverageMinusHighestLowest (Devyash Lodha)

public class AverageMinusHighestLowest_DL extends ConsoleProgram {
	AdvancedFunctions kit = new AdvancedFunctions();
	Vector<Double> nums = new Vector<Double>(7, 1);
	public void run() {
		setSize(640, 480);
		while(true) {
			//ask for numbers
			for(int z = 0; z < 7; z++) {
				println("# Please enter a number!");
				nums.add(readDouble("#\t==>:"));
			}
			int min = 0;
			int max = 0;
			//find the min and max
			for(int x = 0; x < nums.size(); x++) {
				if(nums.elementAt(x) > nums.elementAt(max)) {
					max = x;
				}
				if(nums.elementAt(x) < nums.elementAt(min)) {
					min = x;
				}
			}
			//get the sum
			double fullsum = 0;
			for(int i = 0; i < nums.size(); i++) {
				fullsum+=nums.elementAt(i);
			}
			//get the min and maxnumber
			double minNumber = nums.elementAt(min);
			double maxNumber = nums.elementAt(max);
			//remove the min and max numbers
			nums.remove(max);
			nums.remove(min);
			double avg = 0;
			double sum = 0;
			//calculate the average
			for(int i = 0; i < nums.size(); i++) {
				sum+=nums.elementAt(i);
			}
			avg = sum / nums.size();
			//print results
			println("# MEAN\t:==>:" + avg + ":<==:");
			println("# SUM\t:==>:" + fullsum + ":<==:");
			println("# SUM MIN\t:==>:" + sum + ":<==:");
			println("# LOWEST\t:==>:" + minNumber + ":<==:");
			println("# HIGHEST\t:==>:" + maxNumber + ":<==:");
			println("# END!");
		}
	}
}
