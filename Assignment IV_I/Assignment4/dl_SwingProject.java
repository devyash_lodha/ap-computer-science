import acm.program.*;
import acm.graphics.*;
import acm.util.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class dl_SwingProject extends GraphicsProgram
{
	private JButton one;
	private JButton two;
	private JButton three;
	private GLabel label = new GLabel("No event triggered yet :(", 100, 100);
	GImage memeface = new GImage("/host/share/saves/image1804289383.jpg");
	RandomGenerator rgen = RandomGenerator.getInstance();
	
	public void init()
	{
		setSize(1000, 600);
		label.setFont("Courier-24");
		memeface.setLocation(0, 0);
		memeface.setSize(1000, 600);
		label.setColor(Color.RED);
		add(label);
		one = new JButton("one");
		two = new JButton("Two");
		three = new JButton("Three");
		
		add(one, SOUTH);
		add(two, SOUTH);
		add(three, SOUTH);
		
		this.addActionListeners();
		this.addMouseListeners();
	}
	
	public void run()
	{
		while(true)
		{
			label.setColor(rgen.nextColor());
			label.setLocation(rgen.nextInt(16, 350), rgen.nextInt(16, 350));
			pause(rgen.nextInt(100, 500));
		}
	}
	
	public void actionPerformed(ActionEvent e)
	{
		Object clicked = e.getSource();
		if(clicked == one)
		{
			label.setColor(rgen.nextColor());
			label.setLabel("One! Now, click three or you die!");
		} else {
			if(clicked == two)
			{
				remove(label);
				add(memeface);
			} else {
				if(clicked == three)
				{
					label.setColor(rgen.nextColor());
					label.setLabel("Ahh please! You're good at following directions! Now go and tear your hair out! While pleasing yourself doing that, press button two!");
				}
			}
		}
	}
}