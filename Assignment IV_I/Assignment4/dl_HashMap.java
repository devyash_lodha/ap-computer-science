import java.util.*;
import acm.program.*;
import acm.util.*;

public class dl_HashMap extends ConsoleProgram
{
	private Map data = new HashMap();
	private RandomGenerator rgen = RandomGenerator.getInstance();
	
	public void init()
	{
		setSize(640, 480);
	}
	
	public void run()
	{
		getInformation();
		prompt();
	}
	
	private void getInformation()
	{
		while(true)
		{
			String name = readLine("Please add a person by entering their name. If no more names, enter \"@DONE\"!\n==>:");
			if(name.equalsIgnoreCase("@DONE"))
			{
				break;
			}
			
			while(true)
			{
				String grade = readLine("Please enter grades, on by one! \"@DONE\" to finish!\n==>:");
				if(grade.equalsIgnoreCase("@DONE"))
				{
					break;
				}
				Integer gradeInt = Integer.parseInt(grade);
				
				data.put(name, gradeInt);
			}
		}
	}
	
	private void prompt()
	{
		while(true)
		{
			String command = readLine("Please enter a name to list the grades of! \"@DONE\" to exit!\n==>:");
			if(command.equalsIgnoreCase("@DONE"))
			{
				break;
			}
			
			Iterator iterx = data.keySet().iterator();
			
			while(iterx.hasNext())
			{
				Integer in = (Integer) iterx.next();
				println(in);
			}
		}
	}
}
